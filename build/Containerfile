FROM registry.gitlab.com/grunlab/base-image/debian:12

LABEL maintainer="Adrien Gruneisen <adrien.gruneisen@free.fr>"

USER root

RUN apt-get update && \
    apt-get install --yes php php-mysql && \
    echo "ServerName localhost" >> /etc/apache2/apache2.conf && \
    sed -i 's/Listen 80/Listen 8080/g' /etc/apache2/ports.conf && \
    sed -i 's/Listen 443/Listen 8443/g' /etc/apache2/ports.conf && \
    sed -i 's/:80/:8080/g' /etc/apache2/sites-enabled/000-default.conf && \
    sed -i '/^\tOptions Indexes FollowSymLinks$/!b;n;c\\tAllowOverride All' /etc/apache2/apache2.conf && \
    sed -i 's/www-data/k8s/g' /etc/apache2/envvars && \
    chown -R k8s:k8s /var/run/apache2 /var/log/apache2 /var/www/html && \
    ln -sfT /dev/stderr /var/log/apache2/error.log && \
    ln -sfT /dev/stdout /var/log/apache2/access.log && \
    rm -rf /var/www/html/index.html /var/lib/apt/lists/*

USER k8s

WORKDIR /var/www/html

VOLUME /var/www/html

EXPOSE 8080/tcp 8443/tcp

ENTRYPOINT ["apache2ctl"]

CMD ["-D", "FOREGROUND"]