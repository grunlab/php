[![pipeline status](https://gitlab.com/grunlab/php/badges/main/pipeline.svg)](https://gitlab.com/grunlab/php/-/commits/main)

# GrunLab PHP

PHP non-root container image.

Docs: https://docs.grunlab.net/images/php.md

Base image: [grunlab/base-image/debian:12][base-image]

Format: docker

Supported architecture(s):
- arm64

GrunLab project(s) using this image:
- [grunlab/vigixplorer][vigixplorer]
- [grunlab/zoneminder][zoneminder]

[base-image]: <https://gitlab.com/grunlab/base-image>
[vigixplorer]: <https://gitlab.com/grunlab/vigixplorer>
[zoneminder]: <https://gitlab.com/grunlab/zoneminder>